<!DOCTYPE html>
<html>
<head>
<title>Buat halaman form</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="post">
@csrf
    <label for="fname">First name:</label><br><br>
    <input type="text" id="fname" name="fname"><br><br>
    <label for="lname">Last name:</label><br><br>
    <input type="text" id="lname" name="lname"><br><br>

    <label>Gender:</label><br><br>
    <input type="radio" id="male" name="gender" value="Male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="Female">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender" value="Other">
    <label for="other">Other</label><br><br>

    <label>Nationality:</label><br><br>
    <select>
        <option value="0">Indonesia</option>
        <option value="1">Amerika</option>
        <option value="2">Inggris</option>
    </select><br><br>

    <label>Language Spoken:</label><br><br>
    <input type="checkbox" name="l_sopk" value="0"> Bahasa Indonesia <br>
    <input type="checkbox" name="l_spok" value="1"> English <br>
    <input type="checkbox" name="l_spok" value="2"> Other <br><br>
   

    <label for="bio">Bio:</label><br><br>
    <textarea cols="50" rows="10" id="bio"></textarea><br>
    <input type="submit" value="Sign Up">

</form>
</body>
</html>